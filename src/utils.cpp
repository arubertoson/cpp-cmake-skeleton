#include <cpp-skeleton/utils.h>

#include <string>
#include <sstream>

namespace utils
{
  std::string say_hello(const std::string& name){

    std::stringstream out;

    out << "Hello " << name;

    return out.str();
  }
  
} // utils

